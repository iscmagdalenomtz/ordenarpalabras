﻿using DeskOrdenarPalarbras.Data;

using Microsoft.AspNetCore.Components.Forms;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeskOrdenarPalarbras.Pages
{
	public partial class PageOrdenPalabras
	{



		public List<GrupoPalabras> CasosAOrdenar { get; set; }
		public int NoCasos { get; set; }
		protected override void OnInitialized()
		{
			CasosAOrdenar = new();
		}


		private async Task UploadFiles(InputFileChangeEventArgs e)
		{
			var file = e.File;

			var nombre = file.Name;
			var datoos = file.OpenReadStream();
			var tmpLista = new List<GrupoPalabras>();
			using (var reader = new StreamReader(datoos))
			{
				var TodoElTexto = await reader.ReadToEndAsync();
				tmpLista = ParseToGrupoPalabras(TodoElTexto);
			}

			foreach (var Grupo in tmpLista)
			{
				CasosAOrdenar.Add(ValidarOrdenamiento(Grupo));
			}
		}

		public List<GrupoPalabras> ParseToGrupoPalabras(string TodoElTexto)
		{
			var lstGruposPalabras = new List<GrupoPalabras>();
			var Rows = TodoElTexto.Split("\r\n");
			if (Rows.FirstOrDefault().All(char.IsDigit))
			{
				NoCasos = int.Parse(Rows.FirstOrDefault());
			}
			else
			{
				/// No es digito el primer campo
			}


			var tmpGrupoPalabra = new GrupoPalabras();
			foreach (var item in Rows.Skip(1))
			{
				if (item.All(char.IsDigit))
				{
					if (tmpGrupoPalabra.NoPalabras > 0)
					{
						lstGruposPalabras.Add(tmpGrupoPalabra);
						tmpGrupoPalabra = new GrupoPalabras();
					}

					tmpGrupoPalabra.NoPalabras = int.Parse(item);
				}
				else
				{
					tmpGrupoPalabra.Palabras.Add(item);
				}
			}
			lstGruposPalabras.Add(tmpGrupoPalabra);


			return lstGruposPalabras;
		}

		public GrupoPalabras ValidarOrdenamiento(GrupoPalabras grupoPalabras)
		{
			var PalabraPrincipal = string.Empty;
			var tmpListOrdenada = new List<string>();
			var tmpList = new List<string>();

			for (int j = 0; j < grupoPalabras.NoPalabras; j++)
			{
				grupoPalabras.Ordenable = true;
				tmpList = grupoPalabras.Palabras.Clone();

				PalabraPrincipal = grupoPalabras.Palabras[j];
				tmpList.RemoveAt(j);

				tmpListOrdenada = new List<string>();
				tmpListOrdenada.Add(PalabraPrincipal);

				for (int i = 0; i < grupoPalabras.NoPalabras; i++)
				{

					var Ultimaletra = PalabraPrincipal[PalabraPrincipal.Length - 1];

					var index = tmpList.FindIndex(e => e[0] == Ultimaletra);
					if (index < 0)
					{
						grupoPalabras.Ordenable = false;
						break;
					}
					tmpListOrdenada.Add(tmpList[index]);
					PalabraPrincipal = tmpList[index];
					tmpList.RemoveAt(index);

					if (tmpList.Count == 0)
					{
						break;
					}

				}

				if (grupoPalabras.Ordenable)
				{
					grupoPalabras.PalabrasOrdenadas = tmpListOrdenada;
					break;
				}
			}
			return grupoPalabras;
		}


		public void DescargarArchivo()
		{
			string PathDocumentos = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var dir = Path.Combine(PathDocumentos, "OrdenandoPalabras");
			if (!Directory.Exists(dir))
			{
				DirectoryInfo di = Directory.CreateDirectory(dir);
			}

			var TextCompleto = new List<string>();
			foreach (var item in CasosAOrdenar)
			{
				TextCompleto.Add(item.Ordenable ? "Es posible ordenar" : "No es posible ordenar");
			}

			using (StreamWriter outputFile = new StreamWriter(Path.Combine(dir, "output.out.txt")))
			{
				foreach (var line in TextCompleto)
				{
					outputFile.WriteLine(line);
				}
			}

		}
	}

	static class Extensions
	{
		public static List<T> Clone<T>(this List<T> listToClone)
		{
			return listToClone.Where(e => e != null).ToList();
		}
	}


}

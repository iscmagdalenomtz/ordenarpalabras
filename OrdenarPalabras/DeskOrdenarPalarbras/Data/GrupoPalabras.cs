﻿using System.Collections.Generic;

namespace DeskOrdenarPalarbras.Data
{
	public class GrupoPalabras
	{
		public int NoPalabras { get; set; }
		public List<string> Palabras { get; set; }
		public bool Ordenable { get; set; }
		public List<string> PalabrasOrdenadas { get; set; }


		public GrupoPalabras()
		{
			PalabrasOrdenadas = new List<string>();
			Palabras = new List<string>();
		}
	}
}

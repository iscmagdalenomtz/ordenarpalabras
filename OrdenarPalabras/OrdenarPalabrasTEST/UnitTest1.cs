using DeskOrdenarPalarbras.Data;
using DeskOrdenarPalarbras.Pages;

using NUnit.Framework;

using System.Collections.Generic;

namespace OrdenarPalabrasTEST
{
	public class TestsOrdenarPalabras
	{
		[SetUp]
		public void Setup()
		{
		}
		private static readonly object[] _sourceLists =
		{
			new object[] {new List<string>() { "maiz", "zapato"} , 2 },   //case 1
			new object[] {new List<string>() { "salamandra",  "lunes", "arbol", "lunes" },4 },   //case 1
		};

		private static readonly object[] _sourceLists2 =
		{
			new object[] {new List<string>(){ "moneda", "teclado", "olla", "tarjeta" }, 4},   //case 1
		};

		[Test]
		[TestCaseSource("_sourceLists")]
		public void OrdenandoPalabrasCorrectas(List<string> Palabras, int noPalabras)
		{
			PageOrdenPalabras pageOrden = new PageOrdenPalabras();
			GrupoPalabras grupoPalabras = new GrupoPalabras()
			{
				NoPalabras = noPalabras,
				Ordenable = false,
				Palabras = Palabras,
				PalabrasOrdenadas = new List<string>(),
			};

			var test = pageOrden.ValidarOrdenamiento(grupoPalabras);


			Assert.IsTrue(test.Ordenable && test.PalabrasOrdenadas.Count>0);
		}

		[Test]
		[TestCaseSource("_sourceLists2")]
		public void OrdenandoPalabrasInCorrectas(List<string> Palabras, int noPalabras)
		{
			PageOrdenPalabras pageOrden = new PageOrdenPalabras();
			GrupoPalabras grupoPalabras = new GrupoPalabras()
			{
				NoPalabras = 4,
				Ordenable = false,
				Palabras = Palabras,
				PalabrasOrdenadas = new List<string>(),
			};

			var test = pageOrden.ValidarOrdenamiento(grupoPalabras);


			Assert.IsFalse(test.Ordenable && test.PalabrasOrdenadas.Count == 0);
		}
	}
}